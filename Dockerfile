FROM node:8.16-alpine

WORKDIR /usr/src/app
COPY . ./

RUN npm install -g gulp
RUN npm i gulp

EXPOSE 3000

CMD ["npm", "start"]
