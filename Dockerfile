FROM node:8

WORKDIR /usr/src/app
COPY . ./

COPY package*.json ./
#COPY server.js ./

#RUN apk add --no-cache make gcc g++ python3 nodejs




RUN npm install -g gulp
RUN npm install -g bower
RUN npm i gulp
RUN npm install
RUN bower cache clean --allow-root
RUN bower install angular#~1.3.13 --allow-root
RUN bower install angular-route#~1.3.13 --allow-root
RUN bower install angular-sanitize#~1.3.13 --allow-root

RUN rm -rf /var/cache/apk/*

EXPOSE 3000

CMD ["npm", "start"]
