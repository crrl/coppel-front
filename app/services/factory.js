;(function() {
  angular
    .module('boilerplate')
    .factory('usersAPI', usersAPI);

  usersAPI.$inject = ['API_URL', '$resource'];


  function usersAPI(API_URL, $resource) {

    return $resource(API_URL + '/user/:id', {}, {
      put: {
        method: 'PUT'
      }
    });
  }

})();
