;(function() {

  angular
    .module('boilerplate')
    .controller('MainController', MainController);

  MainController.$inject = ['LocalStorage', 'QueryService', '$scope', 'usersAPI', '$location', '$routeParams', '$route'];


  function MainController(LocalStorage, QueryService, $scope, usersAPI, $location, $routeParams, $route) {
    $scope.currentPage = 1;
    function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
  }

    if ($routeParams.id) {
      usersAPI.get({id:$routeParams.id}).$promise
      .then(function (res) {
        $scope.user = res;
        
      });
    } else {
      usersAPI.get({skip: 0}).$promise
      .then(function (res) {
        $scope.users = res.result;
        $scope.count = res.count;
        $scope.maxPage = Math.ceil($scope.count / 5);
      });
    }

        
    $scope.nextPage = function () {
      $scope.currentPage == Math.ceil($scope.count / 5) ? $scope.currentPage : $scope.currentPage++;
      usersAPI.get({skip: ($scope.currentPage - 1) * 5}).$promise
      .then(function (res) {
        $scope.users = res.result;
        $scope.count = res.count;
        $scope.maxPage = Math.ceil($scope.count / 5);
      });
    }

        
    $scope.previousPage = function () {
      ($scope.currentPage == 1 ? $scope.currentPage : $scope.currentPage--);
      usersAPI.get({skip: ($scope.currentPage - 1) * 5}).$promise
      .then(function (res) {
        $scope.users = res.result;
        $scope.count = res.count;
        $scope.maxPage = Math.ceil($scope.count / 5);
      });
    }

    $scope.goToPage = function (page) {
      $scope.currentPage = page;
      usersAPI.get({skip: ($scope.currentPage - 1) * 5}).$promise
      .then(function (res) {
        $scope.users = res.result;
        $scope.count = res.count;
        $scope.maxPage = Math.ceil($scope.count / 5);
      });
    }

    $scope.editUser = function (id) {
      $location.path(`/editar-usuario/${id}`);
    };

    $scope.removeUser = function () {
      usersAPI.delete({id: $scope.currentid}).$promise
      .then(function (res) {
        $route.reload();
      });
    };

    $scope.modifyUser = function () {
      
      if (!$scope.user.nombre || !$scope.user.apellido || !$scope.user.edad 
          || !validateEmail($scope.user.email)) {
        alert('Favor de llenar todos los campos correctamente.');
      } else {
        usersAPI.put({id: $routeParams.id},{
          nombre: $scope.user.nombre,
          apellido: $scope.user.apellido,
          edad: $scope.user.edad,
          email: $scope.user.email
        }).$promise
        .then(function (res) {
          $location.path(`/`);
        });
      }
    };

    $scope.goHome = function () {
      $location.path(`/`);
    };

    $scope.newUser = function () {
      $location.path('/new-user');
    };

    $scope.createUser = function () {
      if (!$scope.user.nombre || !$scope.user.apellido || !$scope.user.edad 
        || !validateEmail($scope.user.email)) {
      alert('Favor de llenar todos los campos correctamente.');
      } else {
        usersAPI.save({
          nombre: $scope.user.nombre,
          apellido: $scope.user.apellido,
          edad: $scope.user.edad,
          email: $scope.user.email
        }).$promise
        .then(function (res) {
          $location.path(`/`);
        });
      }
    };

    $scope.openModal = function (id) {
      $scope.currentid = id;
      $(".modal").addClass("is-active");  
    };

    $scope.closeModal = function () {
      $(".modal").removeClass("is-active");
    };
  }


})();